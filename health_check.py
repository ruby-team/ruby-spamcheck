#!/usr/bin/env python
""" Health check CLI for spam service """
import argparse
import os

import grpc

# from api.v1.spamcheck_pb2 import HealthCheckRequest, HealthCheckResponse
# import api.v1.spamcheck_pb2_grpc as spam_grpc
from api.v1.health_pb2 import HealthCheckRequest, HealthCheckResponse
import api.v1.health_pb2_grpc as health_grpc

SPAMCHECK_GRPC_ADDR = os.environ.get("SPAMCHECK_GRPC_ADDR", "0.0.0.0:8001")


def check_liveness():
    """Check to use for Kubernetes liveness probes"""
    with grpc.insecure_channel(SPAMCHECK_GRPC_ADDR) as channel:
        stub = health_grpc.HealthStub(channel)
        response = stub.Check(HealthCheckRequest())

    if not isinstance(response, HealthCheckResponse):
        raise AttributeError(f"Unexpected response type: {type(response)}")

    if response.status == HealthCheckResponse.NOT_SERVING:
        raise RuntimeError("gRPC service not serving")


def check_readiness():
    """Check to use for Kubernetes readiness probes"""


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--liveness", action="store_true", default=False)
    parser.add_argument("--readiness", action="store_true", default=False)
    args = parser.parse_args()

    if args.liveness:
        check_liveness()
    if args.readiness:
        check_readiness()
