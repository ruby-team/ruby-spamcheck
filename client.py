#!/usr/bin/env python
import grpc
import api.v1.spamcheck_pb2 as spam
import api.v1.spamcheck_pb2_grpc as spam_grpc

generic_ham = {
    "text": "Dependency update needed The dependencies for this application are outdated and need to be updated.",
    "user_in_project": False,
    "project": {
        "project_id": 5841855,
        "project_path": "gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck-trigger",
    },
    "user": {
        "emails": [{"email": "integrationtest@example.com", "verified": True}],
        "username": "IntegrationTest",
        "org": "IntegrationTest",
        "id": 123,
        "abuse_metadata": {
            "account_age": 30,
            "spam_score": 0.12,
        },
    },
}
generic_spam = {
    "text": "watch fifa live stream best live streaming [here](https://livestream.com)",
    "user_in_project": False,
    "project": {
        "project_id": 5841855,
        "project_path": "gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck-trigger",
    },
    "user": {
        "emails": [{"email": "integrationtest@example.com", "verified": True}],
        "username": "IntegrationTest",
        "org": "IntegrationTest",
        "id": 23,
        "abuse_metadata": {
            "account_age": 3,
            "spam_score": 0.32,
        },
    },
}

issue_ham = {
    "title": "Dependency update needed",
    "description": "The dependencies for this application are outdated and need to be updated.",
    "user_in_project": False,
    "project": {
        "project_id": 5841855,
        "project_path": "gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck-trigger",
    },
    "user": {
        "emails": [{"email": "integrationtest@example.com", "verified": True}],
        "username": "IntegrationTest",
        "org": "IntegrationTest",
        "id": 23,
        "abuse_metadata": {
            "account_age": 3,
            "spam_score": 0.32,
        },
    },
}
issue_spam = {
    "title": "watch fifa live stream",
    "description": "best live streaming [here](https://livestream.com)",
    "user_in_project": False,
    "project": {
        "project_id": 5841855,
        "project_path": "gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck-trigger",
    },
    "user": {
        "emails": [{"email": "integrationtest@example.com", "verified": True}],
        "username": "IntegrationTest",
        "org": "IntegrationTest",
        "id": 23,
        "abuse_metadata": {
            "account_age": 3,
            "spam_score": 0.32,
        },
    },
}

snippet_ham = {
    "title": " internet time",
    "description": "Swatch Internet Time (or .beat time) is a decimal time concept introduced in 1998 by the Swatch corporation where instead of hours and minutes, the mean solar day is divided into 1000 parts called .beats, with the same time around the globe.",
    "user": {
        "emails": [{"email": "integrationtest@example.com", "verified": True}],
        "username": "IntegrationTest",
        "org": "IntegrationTest",
        "id": 23,
        "abuse_metadata": {
            "account_age": 3,
            "spam_score": 0.32,
        },
    },
    "files": [
        {
            "path": "beats.py",
        }
    ],
}
snippet_spam = {
    "title": "Deep sea fishing Dubai | deep sea fishing",
    "description": "Beach Riders can be easily accessed from a variety of easily accessible tourist destinations and look to provide you the best [deep sea fishing Dubai](https://www.beachridersdubai.com/activities/sport-deep-sea-fishing-in-dubai/) has to offer.[ Deep sea fishing](https://www.beachridersdubai.com/activities/sport-deep-sea-fishing-in-dubai/) is one of the best activities that you can undertake with your loved ones. ",
    "user": {
        "emails": [{"email": "integrationtest@example.com", "verified": True}],
        "username": "IntegrationTest",
        "org": "IntegrationTest",
        "id": 23,
        "abuse_metadata": {
            "account_age": 3,
            "spam_score": 0.32,
        },
    },
    "files": [
        {
            "path": "snippetfile1.txt",
        }
    ],
}


def generic_request(generic):
    generic = spam.Generic(**generic)
    with grpc.insecure_channel("localhost:8001") as channel:
        stub = spam_grpc.SpamcheckServiceStub(channel)
        response = stub.CheckForSpamGeneric(generic)
    return response


def issue_request(issue):
    issue = spam.Issue(**issue)
    with grpc.insecure_channel("localhost:8001") as channel:
        stub = spam_grpc.SpamcheckServiceStub(channel)
        response = stub.CheckForSpamIssue(issue)
    return response


def snippet_request(snippet):
    issue = spam.Snippet(**snippet)
    with grpc.insecure_channel("localhost:8001") as channel:
        stub = spam_grpc.SpamcheckServiceStub(channel)
        response = stub.CheckForSpamSnippet(issue)
    return response


def print_result(response):
    print(f"Verdict: {response.Verdict.Name(response.verdict)}")
    print(f"Score: {response.score}")
    print(f"Reason: {response.reason}")


def main():
    print("Checking ham generic")
    response = generic_request(generic_ham)
    print_result(response)

    print("\nChecking spam generic")
    response = generic_request(generic_spam)
    print_result(response)

    print("\nChecking ham issue")
    response = issue_request(issue_ham)
    print_result(response)

    print("\nChecking spam issue")
    response = issue_request(issue_spam)
    print_result(response)

    print("\nChecking ham snippet")
    response = snippet_request(snippet_ham)
    print_result(response)

    print("\nChecking spam snippet")
    response = snippet_request(snippet_spam)
    print_result(response)


if __name__ == "__main__":
    main()
