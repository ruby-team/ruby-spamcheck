"""Process an issue to determine if it is spam or not."""
import api.v1.spamcheck_pb2 as spam

from app import logger, ValidationError
from app.spammable import Spammable


log = logger.logger

# Expecting a module to exist in the directory specified by the ml_classifiers config option.
# i.e {ml_classifiers}/issue/ml
try:
    from issue import classifier
except ModuleNotFoundError as exp:
    log.warning("issue ML classifier not loaded", extra={"error": exp})
    classifier = None  # pylint: disable=invalid-name


def validate(issue: spam.Issue) -> None:
    """Validate that issue contains required fields.

    Raises:
        ValidationError
    """
    if not issue.title:
        raise ValidationError("Issue title is required ")


class Issue(Spammable):
    """Analyze a GitLab issue to determine if it is spam."""

    def __init__(self, issue: spam.Issue, context) -> None:
        validate(issue)
        super().__init__(issue, context, classifier)

Issue.set_max_verdict()
