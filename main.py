#!/usr/bin/env python

""" Entry point for spamcheck service. """
from server import server

server.serve()
